USE ModernWays;
ALTER TABLE Huisdieren ADD COLUMN Geluid VARCHAR(20) CHAR SET utf8mb4;
SET SQL_SAFE_UPDATES = 0;
UPDATE Huisdieren 
SET Geluid = 'WAF!' WHERE Soort = 'Hond';
UPDATE Huisdieren
SET Geluid = 'miauwww...' WHERE Soort = 'Kat';
SET SQL_SAFE_UPDATES = 1;
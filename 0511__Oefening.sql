USE ModernWays;
INSERT INTO huisdieren (
Baasje,
Naam,
Leeftijd,
Soort
)
VALUES
('Vincent', 'Misty', '6', 'Hond'),
('Christiane', 'Ming', '8', 'Hond'),
('Esther', 'Bientje', '6', 'Kat'),
('Jommeke', 'Flip', '75', 'Papegaai'),
('Bert', 'Ming', '7', 'Kat'),
('Thaïs', 'Suerta', '2', 'Hond'),
('Lyssa', 'Фёдор', '1', 'Hond');
